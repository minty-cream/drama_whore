import discord
import asyncio
import pprint
import os
import sys
import random
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound

Base = declarative_base()

client = discord.Client()

class Character(Base):
    __tablename__ = 'character'
    id = Column(Integer, primary_key=True)
    player = relationship("Player", back_populates="character", uselist=False)
    user_id = Column(String)
    name = Column(String)
    level = Column(Integer)
    hit_dice = Column(Integer)
    hit_points = Column(Integer)
    strength = Column(Integer)
    dexterity = Column(Integer)
    constitution = Column(Integer)
    intelligence = Column(Integer)
    wisdom = Column(Integer)
    charisma = Column(Integer)

class Player(Base):
    __tablename__ = 'player'
    id = Column(Integer, primary_key=True)
    user_id = Column(String)
    character_id = Column(Integer, ForeignKey('character.id'))
    character = relationship("Character",back_populates="player")

engine = create_engine('sqlite:///character.db')
Base.metadata.create_all(engine)
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()
help_file = """Hi! I'm here to help.
!help - show this file
!create [Character Name], [str] [dex] [con] [int] [wis] [cha] - Create a character in the bots database, then create a Trello card with the necessary template. Skills, Special Abilities, and other things still need to be filled in. Suggested array: 3,2,2,1,1,0
!change [Character Name] - Automatically change your nickname to the Character you want to play as, and load up their stat array.
!roll [Stat] - Roll the 2d6+Stat
!set [Stat] [Value] - Set a speciic stat to a specific value. Also accepts +/- an integer
!level [Value] - Set your level to a specific value. Also accepts +/- an integer
!hp [Value] - Set your hp to a specific value. Also accepts +/- an integer. Leave Value empty to set to max. Use 'roll' as value to roll it.
"""

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if message.content.startswith('!help'):
        await client.send_message(message.channel, help_file)
    elif message.content.startswith('!create'):
        tmp = await client.send_message(message.channel, 'Creating character...')
        _blob = message.content.split(' ',1)
        _data = _blob[1].split(',',1)
        _name = _data[0].strip()
        _stats = _data[1].strip().split(' ',5)
        _stats = list(map(int,_stats))
        print(_stats)
        _character = Character(
                name = _name,
                strength = _stats[0],
                dexterity = _stats[1],
                constitution = _stats[2],
                intelligence = _stats[3],
                wisdom = _stats[4],
                charisma = _stats[5],
                user_id = message.author.id,
                level = 1,
                hit_dice = 1+_stats[2],
                hit_points = 6
                )
        #Add some sort of verifier
        session.add(_character)
        session.commit()
        await client.delete_message(message)
        await client.edit_message(tmp, _character.name+' added to the db.')
    elif message.content.startswith('!change'):
        tmp = await client.send_message(message.channel, 'Changing character...')
        _blob = message.content.strip().split(' ',1)
        _name = _blob[1].strip()
        character = session.query(Character).\
                filter(Character.name == _name and Character.user_id == message.author.id).\
                one()
        try:
            session.query(Player.user_id).\
                    filter(Player.user_id == message.author.id).one()
        except NoResultFound:
            player = Player(user_id = message.author.id)
            session.add(player)
            session.commit()
        session.query(Player).\
                filter(Player.user_id == message.author.id).\
                update({Player.character_id: character.id})
        session.commit()
        await client.change_nickname(message.author, character.name)
        await client.delete_message(message)
        await client.edit_message(tmp, message.author.name+' is now playing as '+character.name)
    elif message.content.startswith('!roll'):
        tmp = await client.send_message(message.channel, 'Rolling...')
        try:
            player = session.query(Player).\
                    filter(Player.user_id == message.author.id).one()
        except NoResultFound:
            await client.delete_message(message)
            await client.edit_message(tmp, message.author.mention+' please set a character using the `!change` command.')
            return
        character = player.character
        _blob = message.content.strip().split(' ',1)
        _shorts = {
                'str': 'strength',
                'dex': 'dexterity',
                'con': 'constitution',
                'int': 'intelligence',
                'wis': 'wisdom',
                'cha': 'charisma'
                }
        _stat = _shorts[_blob[1].strip()]
        _score = getattr(character,_stat)
        _d1 = random.randrange(1,6)
        _d2 = random.randrange(1,6)
        _roll = _d1+_d2+_score
        await client.delete_message(message)
        await client.edit_message(tmp, message.author.mention+': '+character.name+'\'s '+str(_stat)+' = '+str(_score)+'+('+str(_d1)+'+'+str(_d2)+') = '+str(_roll))
    elif message.content.startswith('!set'):
        tmp = await client.send_message(message.channel, 'Setting...')
        _blob = message.content.strip().split(' ',1)
        _stat = _blob[1].strip()
        _value = _blob[2].strip()
        try:
            player = session.query(Player).\
                    filter(Player.user_id == message.author.id).one()
        except NoResultFound:
            await client.delete_message(message)
            await client.edit_message(tmp, message.author.mention+' please set a character using the `!change` command.')
            return
        character = player.character
        try:
            if(_value.startswith('+') or _value.startswith('-')):
                _mod = int(_value)
                _value = getattr(character,_stat)+_mod
            else:
                _value = int(_value)
            setattr(character,_stat,_value)
            session.commit()
        except:
            await client.delete_message(message)
            await client.edit_message(tmp, 'Something went wrong. Sorry.')
            return
        await client.delete_message(message)
        await client.edit_message(tmp, character.name+'\'s '+_stat+' has been updated to be '+_value+'.')
    elif message.content.startswith('!level'):
        tmp = await client.send_message(message.channel, 'Setting...')
        _blob = message.content.strip().split(' ',1)
        _value = _blob[1].strip()
        try:
            player = session.query(Player).\
                    filter(Player.user_id == message.author.id).one()
        except NoResultFound:
            await client.delete_message(message)
            await client.edit_message(tmp, message.author.mention+' please set a character using the `!change` command.')
            return
        character = player.character
        try:
            if(_value.startswith('+') or _value.startswith('-')):
                _mod = int(_value)
                _value = getattr(character,'level')+_mod
            else:
                _value = int(_value)
            setattr(character,'level',_value)
            session.commit()
        except:
            await client.delete_message(message)
            await client.edit_message(tmp, 'Something went wrong. Sorry.')
            return
        await client.delete_message(message)
        await client.edit_message(tmp, character.name+'\'s Level has been updated to be '+_value+'.')
    elif message.content.startswith('!hp'):
        tmp = await client.send_message(message.channel, 'Setting...')
        _blob = message.content.strip().split(' ',1)
        try:
            _value = _blob[1].strip()
        except:
            _value = 'max'
        try:
            player = session.query(Player).\
                    filter(Player.user_id == message.author.id).one()
        except NoResultFound:
            await client.delete_message(message)
            await client.edit_message(tmp, message.author.mention+' please set a character using the `!change` command.')
            return
        character = player.character
        try:
            if(_value.startswith('+') or _value.startswith('-')):
                _mod = int(_value)
                _value = getattr(character,'hit_points')+_mod
            elif(_value.startswith('roll')):
                _hd = getattr(character, 'hit_dice')
                _count = min(getattr(character, 'hit_dice'), getattr(character, 'level'))
                _value=0
                _rolls = []
                for x in range(_hd):
                    _rand = random.randrange(1,6)
                    _rolls.append(_rand)
                _rolls.sort()
                for x in range(_count):
                    _value = value + _rolls.pop()
            elif(_value.startswith('max')):
                _hd = min(getattr(character, 'hit_dice'), getattr(character, 'level'))
                _value = _hd*6
            else:
                _value = int(_value)
            setattr(character,'hit_points',_value)
            session.commit()
        except:
            await client.delete_message(message)
            await client.edit_message(tmp, 'Something went wrong. Sorry.')
            return
        await client.delete_message(message)
        await client.edit_message(tmp, character.name+'\'s HP has been updated to be '+_value+'.')
    else:
        await client.send_message('Sorry. That isn\'t a command. You can see my commands by typing !help')
        pass

client.run('CLIENT_ID')
