help_file = """!help - show this file
!create [Character Name], [str] [dex] [con] [int] [wis] [cha] - Create a character in the bots database, then create a Trello card with the necessary template. Skills, Special Abilities, and other things still need to be filled in.
!change [Character Name] - Automatically change your nickname to the Character you want to play as, and load up their stat array.
!roll [Stat] - Roll the 2d6+Stat
!set [Stat] [Value] - Set a speciic stat to a specific value. Also accepts +/- an integer
!level [Value] - Set your level to a specific value. Also accepts +/- an integer
!hp [Value] - Set your hp to a specific value. Also accepts +/- an integer
"""

print(help_file)
